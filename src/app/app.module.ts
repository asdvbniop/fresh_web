import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes}   from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DialogModule } from '@progress/kendo-angular-dialog';

import { AppComponent } from './app.component';

import { UserListComponent } from './user-list/user-list.component';
import { UserEditorComponent } from './user-editor/user-editor.component';


import {UserService} from "./services/user-service.service";
import {IdocService} from "./services/idoc-service.service";

import { AddressEditorComponent } from './address-editor/address-editor.component';
import { IdocListComponent } from './idoc-list/idoc-list.component';
import { IdocEditorComponent } from './idoc-editor/idoc-editor.component';

const appRoutes: Routes = [
  { path: 'users', component: UserListComponent},
  { path: 'user/:id', component: UserEditorComponent },
  { path: 'idocs/:id', component: IdocListComponent },
  { path: 'idoc', component: IdocEditorComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserEditorComponent,
    AddressEditorComponent,
    IdocListComponent,
    IdocEditorComponent
  ],


  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    IntlModule,
    ButtonsModule,
    DropDownsModule,
    DateInputsModule,
    InputsModule,
    DialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    UserService,
    IdocService],

  bootstrap: [AppComponent]
})
export class AppModule { }
