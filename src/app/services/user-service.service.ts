import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers } from '@angular/http';
import {Observable} from "rxjs/Rx";
import 'rxjs/add/operator/map';
import {User} from "../domain/User";

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  private hostUrl="http://94.176.236.0:8080";

  getUsers(): Observable<User[]> {
    return this.http.get(this.hostUrl+"/users").map(res => res.json());
  }

  getUser(id:number):Observable<User>
  {
    return this.http.get(this.hostUrl+"/user/"+id).map(res => res.json());
  }

  updateUser(user:User):Observable<User>
  {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.patch(this.hostUrl+"/user",user,options).map(res=>res.json());
  }

  tr(str: string):Observable<any> {
    return this.http.post(this.hostUrl+"/tr",str).map(res=>res.text());
  }
}
