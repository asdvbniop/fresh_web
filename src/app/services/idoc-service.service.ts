import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers } from '@angular/http';
import {Observable} from "rxjs/Rx";
import 'rxjs/add/operator/map';
import {InternalDocument} from "../domain/InternalDocument";
import {FileModel} from "../domain/FileModel";

@Injectable()
export class IdocService {

  constructor(private http: Http) { }

  private hostUrl="http://94.176.236.0:8080";

  getDocsForUser(id:number):Observable<InternalDocument[]>
  {
    return this.http.get(this.hostUrl+"/internalDocuments/"+id).map(res => res.json());
  }

  getDoc(id:number):Observable<InternalDocument>
  {
    return this.http.get(this.hostUrl+"/internalDocument/"+id).map(res => res.json());
  }

  // updateUser(user:User):Observable<User>
  // {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   return this.http.patch("http://127.0.0.1:8080/user",user,options).map(res=>res.json());
  // }
  createDoc(doc: InternalDocument):Observable<InternalDocument> {
    return this.http.post(this.hostUrl+"/internalDocument",doc).map(res => res.json());
  }

  updateDoc(doc: InternalDocument):Observable<InternalDocument> {
    return this.http.patch(this.hostUrl+"/internalDocument",doc).map(res => res.json());
  }

  render(id: number):Observable<FileModel> {
    return this.http.get(this.hostUrl+"/internalDocument/render/"+id).map(res=>res.json());
  }

  deleteFile(id: number):Observable<InternalDocument> {
    return this.http.delete(this.hostUrl+"/internalDocument/render/"+id).map(res=>res.json())
  }
}
