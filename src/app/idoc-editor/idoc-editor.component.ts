import {Component, OnInit, Input} from '@angular/core';
import {InternalDocument} from "../domain/InternalDocument";
import {ActivatedRoute, ParamMap, Params, Router} from "@angular/router";
import {IdocService} from "../services/idoc-service.service";
import {InternalDocumentType} from "../domain/InternalDocumentType";
import {User} from "../domain/User";
import {Price} from "../domain/Price";
import {FullAddressData} from "../domain/FullAddressData";
import { formatDate } from '@telerik/kendo-intl';
import {Language} from "../domain/Language";
import {Utils} from "../domain/Utils";
import {FileModel} from "../domain/FileModel";

@Component({
  selector: 'app-idoc-editor',
  templateUrl: './idoc-editor.component.html',
  styleUrls: ['./idoc-editor.component.scss']
})
export class IdocEditorComponent implements OnInit {

  constructor(private service: IdocService, private route: ActivatedRoute, private router: Router) {}

  processRequest(params: ParamMap) {

    if (params.has("id")) {
      this.isNew = false;
      this.service.getDoc(+params.get('id'))
        .subscribe((doc: InternalDocument) => {this.doc = doc;this.setupView();});
    }
    else {
      let type: InternalDocumentType = this.mapServiceFromString(params.get('type'));
      this.doc = new InternalDocument();
      this.isNew = true;
      this.doc.person = new User();
      this.doc.price = new Price();
      this.doc.person.id = +params.get('user');
      this.doc.type = type;
      this.doc.parent = new InternalDocument();
      this.doc.date = new Date();
      this.doc.fromDate = new Date();
      this.doc.toDate = new Date();

      if(params.has('price'))
      {
        this.doc.price=JSON.parse(params.get('price'));
      }

      if (params.has('parent')) {
        this.doc.parent.id = +params.get('parent')
      }

      if (params.has('parentForAcceptance')) {
        this.doc.parentDocForAcceptance = +params.get('parentForAcceptance')
      }
      this.setupView();
    }
  }

  ngOnInit() {
    this.showFromDate = false;
    this.showToDate = false;
    this.showPrice = false;
    this.showInvoices = false;
    this.showCreateInvoice = false;
    this.showCreateAcceptance = false;
    this.isNew = true;
    this.isInvoice=false;

    console.log(this.route);
    this.route.queryParamMap.subscribe(params => this.processRequest(params));
  }

  @Input()
  doc: InternalDocument = new InternalDocument();


  public listItems: Array<string> = ["USD", "EUR", "UAH"];

  showFromDate: boolean = false;
  showToDate: boolean = false;
  showPrice: boolean = false;
  showInvoices: boolean = false;
  isNew: boolean = true;
  showCreateInvoice: boolean = false;
  showCreateAcceptance: boolean = false;
  isInvoice=false;
  fileUrl:string="/";
  filename:string="";

  private mapServiceFromString(value: string): InternalDocumentType {
    if (value == "Service")
      return InternalDocumentType.SERVICE_AGREEMENT;
    if (value == "Space")
      return InternalDocumentType.RENT_SPACE;
    if (value == "Equipment")
      return InternalDocumentType.RENT_EQUIPMENT;
    else
      return InternalDocumentType[value];
  }

  private setupView(): void {
    if(this.doc.file!=null)
    {
      this.filename=this.doc.file.filename;
      this.fileUrl="http://"+location.hostname+":8080/getFile/"+this.doc.file.id;
    }

    //fix type type
    if(typeof this.doc.type ==='number')
    {

    }else
    {
      this.doc.type=InternalDocumentType[this.doc.type as string];
    }

    let type = this.doc.type;

    if (type == InternalDocumentType.SERVICE_AGREEMENT) {
      this.showToDate = true;
      this.showInvoices = true;
      this.showCreateInvoice = true;
      this.showCreateAcceptance = false;
      this.showPrice = false;
      this.isInvoice=false;
    }

    if (type == InternalDocumentType.INVOICE) {
      this.showPrice = true;
      this.showInvoices = false;
      this.showToDate = false;
      this.showCreateInvoice = false;
      this.showCreateAcceptance = true;
      this.isInvoice=true;

      this.infoBlock.name=this.doc.person.personData.fullNameEng;
      this.infoBlock.acc=this.doc.person.personData.usdAcc.account;
      let iName=this.doc.name;
      let iDate=formatDate(new Date(this.doc.date), "dd/MM/yyyy");
      let sName=this.doc.parent.name;
      let sDate=formatDate(new Date(this.doc.parent.date), "dd/MM/yyyy");
      this.infoBlock.dest=`Payment for Invoice #${iName} from ${iDate} , Service agreement #${sName} from ${sDate}`
      this.infoBlock.addr=Utils.asString(this.doc.person.personData.engAddr);
    }

    if (type == InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE) {
      this.showToDate = true;
      this.showInvoices = false;
      this.showCreateInvoice = false;
      this.showCreateAcceptance = false;
      this.showPrice = true;
      this.showFromDate = true;
    }
  }

  public onSubmit(): void {
    if (this.isNew) {
      this.service.createDoc(this.doc).subscribe((res) => console.log(res));
    }
    else {
      this.service.updateDoc(this.doc).subscribe((res) => console.log(res));
    }
  }

  render(): void
  {
    this.service.render(this.doc.id).subscribe((res:FileModel)=>this.fileUrl="http://"+location.hostname+":8080/getFile/"+res.id);
  }

  onSelectInvoice(ch: InternalDocument): void {
    this.router.navigate(['/idoc'], {queryParams: {id: ch.id}});
  }

  createOrEditAcceptance(): void {
    if (this.doc.acceptance != null && this.doc.acceptance.id != null) {
      this.router.navigate(['/idoc'], {queryParams: {id: this.doc.acceptance.id}});
    }
    else {
      this.router.navigate(['/idoc'], {
        queryParams: {
          id: null,
          type: InternalDocumentType[InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE.toString()],
          user: this.doc.person.id,
          parent: this.doc.parent.id,
          parentForAcceptance: this.doc.id,
          price:JSON.stringify(this.doc.price)
        }
      })
    }
  }

  createInvoice(): void {
    this.router.navigate(['/idoc'], {
      queryParams: {
        id: null,
        type: InternalDocumentType.INVOICE.toString(),
        user: this.doc.person.id,
        parent: this.doc.id
      }
    })
  }

  generateName():void {
     let type:InternalDocumentType = this.doc.type;
    if(type == InternalDocumentType.SERVICE_AGREEMENT)
      this.generateServiceName();
    else if (type == InternalDocumentType.INVOICE)
      this.generateInvoiceName()
  }

  private generateInvoiceName():void{
    this.doc.name="CCIT-"+formatDate(this.doc.date, "MM/yy");
  }

  private generateServiceName():void{
    this.doc.name="CCIT-"+formatDate(this.doc.date, "ddMM/yy");
  }

  onDate(date:Date):void{
    this.generateName();
    this.infoBlock.name=this.doc.name;
  }

  deleteFile():void{
    this.service.deleteFile(this.doc.id).subscribe((doc: InternalDocument) => {this.doc = doc;this.setupView();})
  }

  infoBlock={name:"",acc:"",dest:"",addr:""};


  public opened: boolean = false;

  public close(status : string) {
    console.log(`Dialog result: ${status}`);
    this.opened = false;
  }

  public open() {
    this.opened = true;
  }
}


