import { Component, OnInit, Input } from '@angular/core';
import {InternalDocument} from "../domain/InternalDocument";
import {IdocService} from "../services/idoc-service.service";
import {ActivatedRoute, Router, Params} from "@angular/router";

@Component({
  selector: 'app-idoc-list',
  templateUrl: './idoc-list.component.html',
  styleUrls: ['./idoc-list.component.css']
})
export class IdocListComponent implements OnInit {

  constructor(private service: IdocService,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log("call ngOnInit()");
    this.route.params
      .switchMap((params: Params) => {this.userId = +params['id'];return this.service.getDocsForUser(+params['id'])})
      .subscribe((docs:InternalDocument[])=>this.docs=docs);
  }

  @Input()
  private docs: InternalDocument[] = [];
  public listItems: Array<string> = ["Service", "Space", "Equipment"];
  public newDocType:string;

  @Input()
  public userId;

  onSelect(doc: InternalDocument): void {
    this.router.navigate(['/idoc'],{queryParams: {id:doc.id}});
  }

  createNewDocument(event:any):void{
    this.router.navigate(['/idoc'],{queryParams: {type:event, user:this.userId, id:null}})
  }
}
