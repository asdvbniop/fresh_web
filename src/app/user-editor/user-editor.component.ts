import {Component, OnInit, Input} from '@angular/core';
import {UserService} from "../services/user-service.service";
import {User} from "../domain/User";
import {UserFullData} from "../domain/UserFullData";
import {ActivatedRoute, Params, Router} from "@angular/router";
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-user-editor',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.css']
})
export class UserEditorComponent implements OnInit {
  constructor(private service: UserService,private route: ActivatedRoute, private router: Router) {
    route.params
      .switchMap((params: Params) => this.service.getUser(+params['id']))
      .subscribe((user:User)=>this.setUser(user));
  }

  ngOnInit() {
    // this.
  }

  setUser(user:User)
  {
    console.log(user)
    this.user=user;
  }

  submitData(){
    console.log(this.user);
    this.service.updateUser(this.user).subscribe(
      ()=>this.router.navigate(['/users'])
    );
  }

  @Input()
  user: User=new User();

  trRusName():void
  {
    this.service.tr(this.user.personData.fullNameRus).subscribe(result=>this.user.personData.fullNameEng=result);
  }
}
