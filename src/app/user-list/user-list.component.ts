import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../services/user-service.service";
import {User} from "../domain/User";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  private users: User[] = [];
  constructor(private router: Router, private service: UserService) {}

  ngOnInit() {
    this.service.getUsers().subscribe(
    people=>this.users=people,
    error=>this.users=[]);}

  onSelect(user: User): void {
    this.router.navigate(['/user', user.id]);
  }
}


