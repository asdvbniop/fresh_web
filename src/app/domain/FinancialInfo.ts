import {Currency} from "./Currency";
import {Bank} from "./Bank";

export class FinancialInfo {
  id:number;
  name:string;
  account:string;
  currency:Currency;
  bank:Bank;
}
