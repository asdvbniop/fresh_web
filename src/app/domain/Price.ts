
import {Currency} from "./Currency";
export class Price {
  value: number;
  currency: Currency;
}
