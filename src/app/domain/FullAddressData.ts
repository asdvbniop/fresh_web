import {Language} from "./Language";
export class FullAddressData {
  id:number;
  name:string;
  index:string;
  country:string;
  city:string;
  district:string;
  street:string;
  flat:string;
}
