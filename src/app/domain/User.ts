import {UserFullData} from "./UserFullData";
export class User {
  constructor(){this.personData=new UserFullData();}
  id: number;
  username: string;
  personData : UserFullData;
}
