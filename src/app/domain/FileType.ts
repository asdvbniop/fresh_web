export const enum FileType
{
  PDF,
  JPG,
  PNG,
  DOC
}
