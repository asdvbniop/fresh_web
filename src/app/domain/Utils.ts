import {FullAddressData} from "./FullAddressData";
export class Utils{
  static asString(addr:FullAddressData):string{
    if(addr.flat.length === 0 )
    {
      return addr.street+", "+addr.city+", "+addr.index+", "+addr.country;
    }else{
      return addr.street+", flat "+addr.flat+", "+addr.city+", "+addr.index+", "+addr.country;
    }

  }

}
