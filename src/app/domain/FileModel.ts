
import {Entity} from "./Entity";
import {FileType} from "./FileType";
export class FileModel {
  id: number;
  parentId:number;
  parentType:Entity;
  fileType:FileType;
  filename:string;
  path:string;
  created:Date;
  updated:Date;
  archived:Date;
}
