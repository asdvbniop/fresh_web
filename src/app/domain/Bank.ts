import {FullAddressData} from "./FullAddressData";
export class Bank {
  id: number;
  name:string;
  swift:string;
  mfo:string;
  edr:string;

  rus:FullAddressData;
  eng:FullAddressData;
  ua:FullAddressData;
}
