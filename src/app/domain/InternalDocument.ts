import {Price} from "./Price";
import {User} from "./User";
import {InternalDocumentType} from "./InternalDocumentType";
import {FileModel} from "./FileModel";

export class InternalDocument {
  id: number;
  name:string;
  price:Price;
  date:Date;
  fromDate:Date;
  toDate:Date;
  type:InternalDocumentType;
  templateName:string;
  person:User;
  customer:User;
  file:FileModel;
  children:Array<InternalDocument>;
  parent:InternalDocument;
  acceptance:InternalDocument;
  parentDocForAcceptance:number

}
