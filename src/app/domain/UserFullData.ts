import {FullAddressData} from "./FullAddressData";
import {FinancialInfo} from "./FinancialInfo";

export class UserFullData {
  constructor()
  {
    this.usdAcc=new FinancialInfo();
    this.eurAcc=new FinancialInfo();
    this.uahAcc=new FinancialInfo();

    this.engAddr= new FullAddressData();
    this.rusAddr= new FullAddressData();
    this.uaAddr= new FullAddressData();
  }
  id:number;
  fullNameEng:string;
  fullNameRus:string;
  fullNameUa:string;
  phone:string;
  taxNumber:string;

  engAddr:FullAddressData;
  rusAddr:FullAddressData;
  uaAddr:FullAddressData;

  usdAcc:FinancialInfo;
  eurAcc:FinancialInfo;
  uahAcc:FinancialInfo;
}
