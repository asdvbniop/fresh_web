import {Component, OnInit, Input} from '@angular/core';
import {FullAddressData} from "../domain/FullAddressData";

@Component({
  selector: 'app-address-editor',
  templateUrl: './address-editor.component.html',
  styleUrls: ['./address-editor.component.css']
})
export class AddressEditorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input()
  engAddr:FullAddressData=new FullAddressData();
  @Input()
  rusAddr:FullAddressData=new FullAddressData();
  @Input()
  uaAddr:FullAddressData=new FullAddressData();
}
