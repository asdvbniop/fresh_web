import { FRESHWEBPage } from './app.po';

describe('freshweb App', () => {
  let page: FRESHWEBPage;

  beforeEach(() => {
    page = new FRESHWEBPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
